package com.example.gillesmajor.mymediapp.Model;

import java.util.ArrayList;

/**
 * Created by gillesmajor on 23/07/14.
 * EntriesContainer will return a list of all entries fetched from the local database
 */
public class EntriesContainer {

    // Constructors
    public EntriesContainer(){
        mEntries = new ArrayList<Entry>();
    }
    public EntriesContainer(ArrayList<Entry> entries) {
        mEntries = entries;
    }

    // Class vars
    private static EntriesContainer sEntriesContainer;

    // Instance vars
    private ArrayList<Entry> mEntries;

    // Instance methods

    private void fetchEntriesFromDb(){

           //todo
    }

    // Class methods
    // Get singleton reference to the array containing the entries
    public static EntriesContainer getEntries() {
        if (sEntriesContainer == null) {
            sEntriesContainer = new EntriesContainer();
        }
        return sEntriesContainer;
    }
}
