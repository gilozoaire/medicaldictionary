package com.example.gillesmajor.mymediapp.Model;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by gillesmajor on 24/07/14.
 */
public class JSONReaderHelper {
    // Instance variables
    private JsonReader mJsonReader;

    // Constructors

    public JSONReaderHelper() {
        InputStream in = new InputStream() {
            @Override
            public int read() throws IOException {
                return 0;
            }
        };
        try {
            mJsonReader = new JsonReader(new InputStreamReader(in, "UTF-8"));
        }
        catch (UnsupportedEncodingException e) {
            System.out.println("Unsupported encoding in JSONReader Helper constructor");
            e.getMessage();
        }
    }

    // Public instance methods
    public void startReadingJson() {
        try {
            this.readEntriesArray();
        }
        catch (IOException e) {
            System.out.println("Failed reading array");
        }
    }


    private ArrayList readEntriesArray() throws IOException {
        ArrayList readEntry= new ArrayList();

        mJsonReader.beginArray();
        while (mJsonReader.hasNext()) {
            readEntry.add(readEntry(mJsonReader));
        }
        mJsonReader.endArray();
        return readEntry;
    }

    public Entry readEntry() throws  IOException {
        String group = null;
        ArrayList<Translation> translations = null;
    }

}
