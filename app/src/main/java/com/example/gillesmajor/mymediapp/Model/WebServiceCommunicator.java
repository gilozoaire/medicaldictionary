package com.example.gillesmajor.mymediapp.Model;

import java.net.URI;
import java.util.ArrayList;

import com.example.gillesmajor.mymediapp.Model.Entry;
import com.example.gillesmajor.mymediapp.Model.RequestAsyncTask;

/**
 * Created by gillesmajor on 23/07/14.
 */
public class WebServiceCommunicator implements RequestAsyncTask.RequestAsyncTaskDelegate {

    // Instance variables
    private URI mURI;
    private String rawJSON;
    private ArrayList<Entry> mEntries;

    // Getters and setters
    public URI getURL() {
        return mURI;
    }

    public void setURL(URI uri) {
        mURI = uri;
    }

    public String getRawJSON() {
        return rawJSON;
    }

    public void setRawJSON(String rawJSON) {
        this.rawJSON = rawJSON;
    }

    // Constructors
    public WebServiceCommunicator(URI uri) {
        mURI = uri;
    }

    // Instance methods
    public void fetchDataFromWebService() {
        URI[] uris = {mURI};
        RequestAsyncTask request = new RequestAsyncTask(this);
        request.execute(uris);
    }

    // Class methods

    // RequestAsyncDelegate
    @Override
    public void requestFinishedFetchingData(String s) {
        this.setRawJSON(s);
    }
}
