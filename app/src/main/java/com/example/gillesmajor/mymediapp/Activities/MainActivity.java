package com.example.gillesmajor.mymediapp.Activities;

import android.os.Bundle;
import android.app.ActionBar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.example.gillesmajor.mymediapp.Model.Entry;
import com.example.gillesmajor.mymediapp.R;
import com.example.gillesmajor.mymediapp.Adapters.TabsPagerAdapter;
import com.example.gillesmajor.mymediapp.Model.WebServiceCommunicator;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;


public class MainActivity extends FragmentActivity implements ActionBar.TabListener {
    private ViewPager mViewPager;
    private TabsPagerAdapter mTabsPagerAdapter;
    private ActionBar mActionBar;
    private ArrayList<Entry> mEntries;

    // Tab titles
    private static String[] tabTitles;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        // Initilization
        tabTitles = getResources().getStringArray(R.array.tabStrings);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mActionBar = getActionBar();
        mTabsPagerAdapter = new TabsPagerAdapter(getSupportFragmentManager());

        mViewPager.setAdapter(mTabsPagerAdapter);
        mActionBar.setHomeButtonEnabled(false);
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Add tabs
        String[] tabs = getResources().getStringArray(R.array.tabStrings);
        for (String tabName : tabs) {
            mActionBar.addTab(mActionBar.newTab().setText(tabName).setTabListener(this));
        }

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // empty
            }

            @Override
            public void onPageSelected(int position) {
                // When the user switches the view by swiping, reflect the change in the selected tab
                mActionBar.setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // empty
            }
        });


        // Fetch JSON string from the webservice
        String uriStr = getResources().getString(R.string.webServiceUrl);
        URI webServUri = null;

        try {
            webServUri = new URI(uriStr);
        }
        catch (URISyntaxException e) {
            e.printStackTrace();
        }

        WebServiceCommunicator communicator = new WebServiceCommunicator(webServUri);
        communicator.fetchDataFromWebService();


        //System.out.println( communicator.getRawJSON());

        // Insert data in the database
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {

    }
    @Override
    public void onTabSelected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {
        // show the appropriate fragment when clicking the tab
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {

    }
}
