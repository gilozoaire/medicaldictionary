package com.example.gillesmajor.mymediapp.Fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.gillesmajor.mymediapp.R;


public class FavouritesFragment extends android.support.v4.app.Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View favView = inflater.inflate(R.layout.fragment_favourites, container, false);
        return favView;
    }
}
