package com.example.gillesmajor.mymediapp.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by gillesmajor on 10/07/14.
 */

import com.example.gillesmajor.mymediapp.Fragments.FavouritesFragment;
import com.example.gillesmajor.mymediapp.Fragments.HistoryFragment;
import com.example.gillesmajor.mymediapp.Fragments.SearchFragment;
import com.example.gillesmajor.mymediapp.Fragments.SettingsFragment;

public class TabsPagerAdapter extends FragmentPagerAdapter {
    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {
        switch (index) {
            case 0: return new SearchFragment();
            case 1: return new FavouritesFragment();
            case 2: return new HistoryFragment();
            case 3: return new SettingsFragment();
            default: return null;
        }
    }
    @Override
    public int getCount() {
        return 4;
    }
}
