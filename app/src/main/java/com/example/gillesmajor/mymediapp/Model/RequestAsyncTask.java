package com.example.gillesmajor.mymediapp.Model;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;

/**
 * Created by gillesmajor on 23/07/14.
 */
public class RequestAsyncTask extends AsyncTask<URI, Integer, String> {

    private WebServiceCommunicator mWebServiceCommunicator;

    // Constructors

    public RequestAsyncTask(WebServiceCommunicator webServiceCommunicator) {
        mWebServiceCommunicator = webServiceCommunicator;
    }


    // Getters and setters

    public WebServiceCommunicator getWebServiceCommunicator() {
        return mWebServiceCommunicator;
    }

    public void setWebServiceCommunicator(WebServiceCommunicator webServiceCommunicator) {
        mWebServiceCommunicator = webServiceCommunicator;
    }


    // AsyncTask methods

    @Override
    protected String doInBackground(URI... uris) {
        HttpResponse httpResponse;
        String responseString = null;
        try {
            URI uri = uris[0];
            HttpGet httpGet = new HttpGet(uri);
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 5000);
            httpGet.setParams(params);

            HttpClient client = new DefaultHttpClient(params);
            httpResponse = client.execute(httpGet);

            StatusLine statusLine = httpResponse.getStatusLine();
            System.out.println("Test test test" + statusLine);

            if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                httpResponse.getEntity().writeTo(outputStream);
                outputStream.close();
                responseString = outputStream.toString();
                System.out.println(responseString);
            }
            else {
                // Closes the connection
                httpResponse.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        }
        catch (ClientProtocolException e) {
            System.out.println("Client Protocol Exception");
            e.printStackTrace();
        }
        catch (IOException e) {
            System.out.println("IOException occurred in request async");
            e.printStackTrace();
        }
        catch (Exception e) {
            System.out.println("Generic exception has occurred");
            e.printStackTrace();
        }

        return responseString;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        // pass the rawJson to the WebCommunicator
        getWebServiceCommunicator().requestFinishedFetchingData(s);
    }

    // Delegate Interface
    public interface RequestAsyncTaskDelegate {
        public void requestFinishedFetchingData(String s);
    }
}
