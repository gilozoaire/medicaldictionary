package com.example.gillesmajor.mymediapp.Model;

import java.lang.reflect.Array;

/**
 * Created by gillesmajor on 10/07/14.
 */
public class Entry {
    private Array frenchStrings, englishStrings, dutchStrings;
    private String category, group;

    // Constructor
    public Entry(Array frenchStrings, Array englishStrings, Array dutchStrings) {
        this.frenchStrings = frenchStrings;
        this.englishStrings = englishStrings;
        this.dutchStrings = dutchStrings;
    }

    // Getters and setters
    public Array getFrenchStrings() {
        return frenchStrings;
    }

    public void setFrenchStrings(Array frenchStrings) {
        this.frenchStrings = frenchStrings;
    }

    public Array getEnglishStrings() {
        return englishStrings;
    }

    public void setEnglishStrings(Array englishStrings) {
        this.englishStrings = englishStrings;
    }

    public Array getDutchStrings() {
        return dutchStrings;
    }

    public void setDutchStrings(Array dutchStrings) {
        this.dutchStrings = dutchStrings;
    }
}
